using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public delegate void StartGame(string startingSide);
public delegate void ChangeSides(string newPlayer);
public delegate void GameOver(string result);
public delegate void RestartGame();

public class GameController : MonoBehaviour {
	
	enum GameState{Waiting,Playing,GameOver};
	
	public Text []_GridTextList;
	public static event StartGame _OnStartGame;
	public static event ChangeSides _OnChangeSides;
	public static event GameOver _OnGameOver;
	public static event RestartGame _OnRestartGame;
	
	string _PlayerSide;
	int _MoveCount;
	GameState _CurrentState;
	
	void Awake () {
		_MoveCount = 0;
		_CurrentState = GameState.Waiting;
		
	}
	void OnEnable() {
		InputManager._OnGridClicked += GridSpaceClicked;
		InputManager._OnPlayerButtonClicked += PlayerButtonClicked;
		InputManager._OnRestartButtonClicked += RestartGame;
	}
	
	void OnDisable() {
		InputManager._OnGridClicked -= GridSpaceClicked;
		InputManager._OnPlayerButtonClicked -= PlayerButtonClicked;
		InputManager._OnRestartButtonClicked -= RestartGame;
	}
	
	void PlayerButtonClicked(string player) {
		if(_CurrentState == GameState.Waiting) {
			StartGame(player);
		}
	}
	
	void StartGame(string startingside) {
		_PlayerSide = startingside;
		if(_OnStartGame != null)
			_OnStartGame(startingside);
		_CurrentState = GameState.Playing;
	}
	
	void GridSpaceClicked(int i) {
		if(_CurrentState == GameState.Playing && _GridTextList[i].text=="") {
			_GridTextList[i].text = _PlayerSide;
			EndTurn();
		}	
	}
	
	void EndTurn () {
		_MoveCount++;
		if (!CheckGameOver())
			ChangeSides();
	}
	
	bool CheckGameOver() {
		
		// Check Horizontal/Vertical Win Conditions
		for (int i=0; i<3;i++) {
			if ((_GridTextList[3*i].text==_PlayerSide && _GridTextList[3*i+1].text==_PlayerSide && _GridTextList[3*i+2].text==_PlayerSide) ||
			    (_GridTextList[i].text==_PlayerSide && _GridTextList[i+3].text==_PlayerSide && _GridTextList[i+6].text==_PlayerSide) ) 
			{
				GameOver(_PlayerSide + " Won!");
				return true;
			}	
		}
		
		// Check Diagonal Win Conditions
		if ((_GridTextList[0].text==_PlayerSide && _GridTextList[4].text==_PlayerSide && _GridTextList[8].text==_PlayerSide) ||
			(_GridTextList[2].text==_PlayerSide && _GridTextList[4].text==_PlayerSide && _GridTextList[6].text==_PlayerSide) ) 
		{
			GameOver(_PlayerSide + " Won!");
			return true;
		}
		
		// Check if game drawn.
		if (_MoveCount==9) {
			GameOver ("Its a draw..");
			return true;
		}
		
		return false;	
	}
	
	void ChangeSides() {
		if (_PlayerSide=="X")
			_PlayerSide = "O";
		else
			_PlayerSide = "X";
		if(_OnChangeSides != null)
			_OnChangeSides(_PlayerSide);
	}
	
	void GameOver(string result) {
		if(_OnGameOver != null)
			_OnGameOver(result);
		_CurrentState = GameState.GameOver;	
	}
	
	void RestartButtonClicked() {
		if (_CurrentState == GameState.GameOver)
			RestartGame();
	}
	
	void RestartGame() {
		for (int i=0;i<9;i++) {
			_GridTextList[i].text = "";
		}
		if(_OnRestartGame != null)
			_OnRestartGame();
		_MoveCount = 0;
		_CurrentState = GameState.Waiting;
		
	}
	
}
