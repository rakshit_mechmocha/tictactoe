﻿using UnityEngine;
using System.Collections;

public delegate void GridClicked(int i);
public delegate void PlayerButtonClicked(string player);
public delegate void RestartButtonClicked();

public class InputManager : MonoBehaviour {

	public RectTransform _PlayerXRectTransform;
	public RectTransform _PlayerORectTransform;
	public RectTransform _RestartButtonRectTransform;
	public RectTransform[] _GridSpaceRectTransform;
	
	public static event GridClicked _OnGridClicked;
	public static event PlayerButtonClicked _OnPlayerButtonClicked;
	public static event RestartButtonClicked _OnRestartButtonClicked;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			
			if(RectTransformUtility.RectangleContainsScreenPoint(_PlayerXRectTransform,Input.mousePosition,Camera.main) ) {
				if(_OnPlayerButtonClicked != null)
					_OnPlayerButtonClicked("X");
			}
			if(RectTransformUtility.RectangleContainsScreenPoint(_PlayerORectTransform,Input.mousePosition,Camera.main) ) {
				if(_OnPlayerButtonClicked != null)
					_OnPlayerButtonClicked("O");
			}
			if(RectTransformUtility.RectangleContainsScreenPoint(_RestartButtonRectTransform,Input.mousePosition,Camera.main) ) {
				if(_OnRestartButtonClicked != null)
					_OnRestartButtonClicked();
			}
			for (int i=0;i<9;i++) {
				if(RectTransformUtility.RectangleContainsScreenPoint(_GridSpaceRectTransform[i],Input.mousePosition,Camera.main) ) {
					if(_OnGridClicked != null)
						_OnGridClicked(i);
				}
			}
		}
	}
}
