﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class PlayerColor {
	public Color _PanelColor;
	public Color _TextColor;
}

[System.Serializable]
public class Player {
	public Image _Panel;
	public Text _Text;
}

public class BoardController : MonoBehaviour {
	
	public GameObject _GameOverPanel;
	public Text _GameOverText;
	public GameObject _RestartButton;
	public Player _PlayerX;
	public Player _PlayerO;
	public PlayerColor _ActivePlayerColor;
	public PlayerColor _InactivePlayerColor;
	
	
	void Awake() {
		_GameOverPanel.SetActive(false);
		_RestartButton.SetActive(false);
	}
	
	void OnEnable() {
		GameController._OnStartGame += SetStartingSide;
		GameController._OnChangeSides += ChangeSides;
		GameController._OnGameOver += GameOver;
		GameController._OnRestartGame += CleanBoard;
	}
	
	void OnDisable() {
		GameController._OnStartGame -= SetStartingSide;
		GameController._OnChangeSides -= ChangeSides;
		GameController._OnGameOver -= GameOver;
		GameController._OnRestartGame -= CleanBoard;
	}
	
	void SetStartingSide(string startingSide) {
		if (startingSide == "X") {
			SetPlayerColor(_PlayerX,_PlayerO);
		} else {
			SetPlayerColor(_PlayerO,_PlayerX);
		}
	}
	
	void SetPlayerColor(Player newPlayer, Player oldPlayer) {
		newPlayer._Panel.color = _ActivePlayerColor._PanelColor;
		newPlayer._Text.color = _ActivePlayerColor._TextColor;
		oldPlayer._Panel.color = _InactivePlayerColor._PanelColor;
		oldPlayer._Text.color = _InactivePlayerColor._TextColor;
	}
	
	void SetPlayerColorsInactive() {
		_PlayerX._Panel.color = _InactivePlayerColor._PanelColor;
		_PlayerX._Text.color = _InactivePlayerColor._TextColor;
		_PlayerO._Panel.color = _InactivePlayerColor._PanelColor;
		_PlayerO._Text.color = _InactivePlayerColor._TextColor;
	}
	void CleanBoard() {
		_GameOverPanel.SetActive(false);
		_RestartButton.SetActive(false);
		SetPlayerColorsInactive();
	}
	
	void ChangeSides(string newSide) {
		if(newSide == "O")
			SetPlayerColor(_PlayerO,_PlayerX);
		else
			SetPlayerColor(_PlayerX,_PlayerO);
	}
	
	void GameOver(string result) {
		_GameOverPanel.SetActive(true);
		_RestartButton.SetActive(true);
		_GameOverText.text = result;
	}
}
